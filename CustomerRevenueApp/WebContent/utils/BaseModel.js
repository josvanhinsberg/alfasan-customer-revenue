jQuery.sap.require("nl.alfasan.customerrevenue.library.ui.model.json.JSONModel");
sap.ui.define(["com/barrydam/sap/ui/model/json/JSONModel" ], function(BdJSONModel) {
	
	var sRootPath = jQuery.sap.getModulePath("nl.alfasan.customerrevenue").replace(".","");
	var sLoad = sRootPath+'/';
	if (document.location.pathname == '/Alfasan/customerrevenue/') {
		sLoad = '.'+sLoad;
	} else if (document.location.hostname === "localhost") {
		sLoad = document.location.pathname ;
	}

	var Extend = BdJSONModel.extend("nl.alfasan.customerrevenue.utils.BaseModel", {		
		bindTo : function(o, sName) {
			o.setModel(this, sName);
		},
		loadJSON: function(sPath, mParams, bSynchronous) {
			mParams = mParams || {};
			this.loadData(
				sLoad+sPath,
				mParams,
				false//bSynchronous // false for synchronously loading
			);
		}
	});

	return Extend;
});