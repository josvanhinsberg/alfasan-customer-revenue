sap.ui.define([  
	'sap/ui/core/mvc/Controller',
	'sap/ui/core/routing/History'
], function(Controller, History) {

	/**
	 * The basecontroler helds commonly used functions
	 */
	return Controller.extend('nl.alfasan.customerrevenue.utils.BaseController', {
		/**
		 * Router methods
		 */
		_getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},
		_navTo : function(to, object, bReplaceHash) {
			this._getRouter().navTo(to, object, bReplaceHash);
		},
		_onNavBack : function() {
			if (History.getInstance().getPreviousHash() !== undefined) {
				window.history.go(-1);
			} else {
				this.navTo('Events');
			}
		},


		/**
		 * Translations
		 */
		_i18n: function(sResourceText) {
			var sTextFromResource = this.getOwnerComponent().getModel("i18n").getProperty(sResourceText);
			// if any additional args are passed... we can replace placeholders in the i18n
			if (arguments.length > 1) {
				for (var i in arguments) {
					if (i === 0) {
						continue;
					}
					sTextFromResource = sTextFromResource.replace('{'+i+'}', arguments[i]);
				}
			}
			// return new value
			return sTextFromResource;
		},

		/**
		 * Loaders
		 */
		__loaderDialog : false,
		_showLoader: function() {
			if (! this.__loaderDialog) {
				this.__loaderDialog = new sap.m.BusyDialog();
				this.getView().addDependent(this.__loaderDialog);
			}
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.__loaderDialog);
			this.__loaderDialog.open();
			//sap.ui.core.BusyIndicator.show();
		},
		_hideLoader: function() {
			if (this.__loaderDialog) {
				this.__loaderDialog.close();
			}
		}
	});
});