sap.ui.define([
	"./ODataModel",
	"sap/ui/model/json/JSONModel"
], function(ODataService, JSONModel) {
	"use strict";
	
	var _private = {
		/**
		 * Functie om je parameters te mergen / controleren met default parameters
		 * @param {object} mParams - parameters verstuurd bij functie
		 * @param {object} mDefault	- default parameters die worden gezet indien een key niet beschikbaar is in mParams
		 */
		parseParams: function(mParams, mDefault) {
			var mParsed = {};		
			if (typeof mParams !== "object") {
				mParsed = mDefault || {};
			} else if (typeof mDefault === "object") {
				for (var key in mDefault) {
					if (key in mParams) { // controleren of functies ook functies zijn
						mParsed[key] = (typeof mDefault[key] === "function" && typeof mParams[key] !== "function") ? function() {} : mParams[key] ;
					} else {
						mParsed[key] = mDefault[key];
					}
				}
			}
			return Object.assign({}, mParsed);
		},
		/**
		 * read uitvoeren
		 */
		readModel: function(oModel, sPath, mParams) {
			var mParsedParams = _private.parseParams(
				mParams,
				{
					success: function() {},
					error: function() {}
				}
			);
			var mRequestParams = {
				success: function(mData) {
					// that._originalData = mData;
					oModel.setData(mData);
					mParsedParams.success(mData);
				},
				error: mParsedParams.error
			};
			if ("urlParameters" in mParams && mParams.urlParameters) {
				mRequestParams.urlParameters = mParams.urlParameters;
			}
			ODataService.read(sPath, mRequestParams);
		}
	};

	var oModel = JSONModel.extend("BaseModel", {
		_loadEvents: [],  // hierin zitten alle callback functies die worden afgevuurd op het moment dat het model geladen is
		_constructorSettings : { path: null, urlParameters: null}, // deze worden gebruikt bij en reload
		
		
		constructor: function(sPath, mParams) {
			JSONModel.apply(this);
			var that = this;
			this._constructorSettings = { 
				path			: sPath, 
				urlParameters	: (typeof mParams === "object" && "urlParameters" in mParams) ? mParams.urlParameters : false
			};
			_private.readModel(this, sPath, {
				success			:  function() {
					if (typeof mParams === "object" && "success" in mParams && typeof mParams.success === "function") {
						 mParams.success.apply(this, arguments);
					} 
					that.fireLoaded();
				},
				error			: (typeof mParams === "object" && "error" in mParams && typeof mParams.error === "function") ? mParams.error : function(){},
				urlParameters	: this._constructorSettings.urlParameters
			});
		},
		
		reload : function(mParams) {
			var mParameters = (typeof mParams === "object") ? mParams : {};
			mParameters.urlParameters = this._constructorSettings.urlParameters;
			_private.readModel(this, this._constructorSettings.path, mParameters);
		},
		
		attachLoaded : function(fnCallback) {
			if (typeof fnCallback !== "function") { return; }
			// Indien de usersettings al geladen zijn dient de callback direct te worden uitgevoerd
			if(Object.keys(this.getProperty("/")).length) {
				fnCallback();
			} else { // anders tijdelijk opslaan zodat deze wordt uitgevoerd als de settings geladen zijn
				this._loadEvents.push(fnCallback);
			}
		},
		
		fireLoaded : function() {
			if (this._loadEvents.length === 0) { return; }
			for (var i in this._loadEvents) {
				if (typeof this._loadEvents[i] === "function") {
					this._loadEvents[i]();
				}
			}
			// tijdelijk opgeslagen events leeg gooien
			this._loadEvents = [];
		}
		
		/** TODO SAVE FUNCTIE */
	});

	return oModel;
});