sap.ui.define([ 
	"sap/ui/model/odata/v2/ODataModel"
], function(ODataModel) {
	"use strict";
	/**
	 * ODataService Model
	 * 
	 * Basis Model voor alle andere modellen
	 * 
	 * @class 		
	 * 
	 * @constructor
	 * @public
	 */
	var oDataService = new ODataModel(
		"/sap/opu/odata/SAP/ZSD_CUST_REVENUE_OVERVIEW_SRV",
		{
			json				: true,
			useBatch			: false,
			defaultBindingMode	: "TwoWay"
		}
	);
	
	return oDataService;
});