sap.ui.define([
	"./BaseModel"
], function(BaseModel) {
	"use strict";

	var RevenueTable = BaseModel.extend("nl.alfasan.customerrevenue.model.RevenueTable", {
		constructor: function(id, mParams) {
			BaseModel.call(this, "/RevenueTableSet('" + id +"')", mParams);
		}			
	
	});
	
	return RevenueTable;
});