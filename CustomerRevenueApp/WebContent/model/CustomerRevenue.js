sap.ui.define([
	"./BaseModel"
], function(BaseModel) {
	"use strict";

	var CustomerRevenue = BaseModel.extend("nl.alfasan.customerrevenue.model.CustomerRevenue", {
		constructor: function(id, mParams) {
			BaseModel.call(this, "/CustomerRevenueListSet('" + id +"')", mParams);
		}			
	
	});
	
	return CustomerRevenue;
});