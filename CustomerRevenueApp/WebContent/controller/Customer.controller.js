sap.ui.define([
	"../utils/BaseController",
	"../model/CustomerRevenue"
], function(BaseController, CustomerRevenueModel) {
	"use strict";
	return BaseController.extend("nl.alfasan.customerrevenue.controller.Customer", {
		
		onInit: function() {
			this._getRouter().attachRouteMatched(this.onRouteMatched, this);
		},

		onRouteMatched: function(e) {
			if (e.getParameter("name") != "Customer") {
				return false;
			}
			var id = e.getParameter("arguments").id || false,
				Customer = new CustomerRevenueModel(id, {
					success: function(m) {
						console.log(m);
						
					}, 
					error: function() {
						
					}
				});
				
			this.getView().setModel(Customer, "Customer");

		}
	});
});