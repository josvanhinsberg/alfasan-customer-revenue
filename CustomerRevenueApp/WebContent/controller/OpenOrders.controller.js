sap.ui.define([
	"../utils/BaseController",
	"../model/ODataModel"
], function(BaseController, ODataModel){
	"use strict";
	
	return BaseController.extend("nl.alfasan.customerrevenue.controller.OpenOrders", {
		
		onInit: function(){
			this._getRouter().attachRouteMatched(this.onRouteMatched, this);
		},
		
		onRouteMatched: function(e){
			if(e.getParameter("name") !== "Customer"){
				return;
			}
			
			//Set the filter value for the OData call
			var id = e.getParameter("arguments").id || false,
				aFilter = [],
				mFilter = new sap.ui.model.Filter(
					"Kunnr", 
					sap.ui.model.FilterOperator.EQ,
					id
				);
			aFilter.push(mFilter);
			var oBinding = this.getView().byId("ordersTable").getBinding("items");
			oBinding.filter(aFilter);
			
			this.getView().setModel(ODataModel, "ODataModel");
		}
	});	
});