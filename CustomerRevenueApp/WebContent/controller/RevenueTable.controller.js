sap.ui.define([
	"../utils/BaseController",
	"../model/ODataModel",
	"sap/ui/model/json/JSONModel"
], function(BaseController, ODataModel, JSONModel) {
	"use strict";
	return BaseController.extend("nl.alfasan.customerrevenue.controller.RevenueTable", {
		
		onInit: function() {
			this._getRouter().attachRouteMatched(this.onRouteMatched, this);
		},

		onRouteMatched: function(e) {
			if (e.getParameter("name") != "Customer") {
				return false;
			}
			
			//Provide the column labels for the years
			var mYear = new Date().getFullYear();
			var lbl0 = this.byId('year0');
			lbl0.setText(mYear);
			mYear = mYear - 1;
			var lbl1 = this.byId('year1');
			lbl1.setText(mYear);
			mYear = mYear - 1;
			var lbl2 = this.byId('year2');
			lbl2.setText(mYear);
			mYear = mYear - 1;
			var lbl3 = this.byId('year3');
			lbl3.setText(mYear);

			//Set the filter value for the OData call
			var id = e.getParameter("arguments").id || false,
				aFilter = [],
				mFilter = new sap.ui.model.Filter(
					"Kunnr", 
					sap.ui.model.FilterOperator.EQ,
					id
				);
			aFilter.push(mFilter);
			var oBinding = this.getView().byId("revenueTable").getBinding("items");
			oBinding.filter(aFilter);
			
			this.getView().setModel(ODataModel, "ODataModel");

		},
		
		_oPricePopover: null,
		onShowPrice: function(e) {
		    // create popover
			if (! this._oPricePopover) {
				this._oPricePopover = sap.ui.xmlfragment("nl.alfasan.customerrevenue.view.RevenueTablePopoverC", this);
				this.getView().addDependent(this._oPopover);
				this._oPricePopover.addStyleClass('sapUiContentPadding');
			}
			if (e.getParameter("type") == "out") {
			    this._oPricePopover.close();
			} else if (e.getParameter("type") == "over") {
                //Get the amount and currency to be shown in the small popover window
				var oButton = e.getSource(),
                    total   = oButton.data("total"),
                	curr	= oButton.data("curr");
    			
    			this._oPricePopover.setModel(new JSONModel({total: total, curr: curr}), "Total");
    			// delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
    			jQuery.sap.delayedCall(0, this, function () {
    				this._oPricePopover.openBy(oButton);
    			});
			}
		},
		
		onShowOverview: function(e) {
            var oBtn    = e.getSource(),
                iYear   = oBtn.data("year"),
                iCust   = oBtn.data("customer"),
                iMat	= oBtn.data("material"),
                iTotal  = oBtn.data("total"),
                iCurr   = oBtn.data("curr");
            var openDialog = $.proxy(this.poDialog.openDialog, this);
            openDialog(iCust, iMat, iYear, iTotal, iCurr);
		},
		
		poDialog: {
			_oDialog: null,
			openDialog: function(cust, mat, year, total, curr){
				//Create the dialog
				var sFragment = "nl.alfasan.customerrevenue.view.RevenueTablePoDialog";
				if(!this.poDialog._oDialog){
					this.poDialog._oDialog = sap.ui.xmlfragment(sFragment, sFragment, this);
				}
				//Set the correct filter values for the table
				var aFilter = [];
				//Add the selected customer to the filter
				var mFilter = new sap.ui.model.Filter(
						"Kunnr", 
						sap.ui.model.FilterOperator.EQ,
						cust
					);
				aFilter.push(mFilter);
				//Add the selected material to the filter
				mFilter = new sap.ui.model.Filter(
						"Matnr", 
						sap.ui.model.FilterOperator.EQ,
						mat
					);
				aFilter.push(mFilter);
				//Add the selected year to the filter
				mFilter = new sap.ui.model.Filter(
							"OrderYear", 
							sap.ui.model.FilterOperator.EQ,
							year
						);
				aFilter.push(mFilter);
				//Set the dialog model
				this.poDialog._oDialog.setModel(ODataModel, "ODataModel");
				switch (year) {
					case "0":
						var mYear = new Date().getFullYear();
						break;
					case "1":
						var mYear = new Date().getFullYear() - 1;
						break;
					case "2": 
						var mYear = new Date().getFullYear() - 2;
						break;
					case "3":
						var mYear = new Date().getFullYear() - 3;
				}			
				this.poDialog._oDialog.setModel(new JSONModel({material: mat, 
															   year: mYear, 
															   total: total, 
															   currency: curr}), 
												"Dialog");
				
				//Add dependency to be able to use the i18n model inside the dialog
				this.getView().addDependent(this.poDialog._oDialog);
				//Now let's open the dialog
				this.poDialog._oDialog.open();
				
				//Link the fragment to its ID
				var oPoTableFilter = sap.ui.getCore().byId(sap.ui.core.Fragment.createId(sFragment , "PoTable"));
				//Apply the filter values
				oPoTableFilter.getBinding("items").filter(aFilter);
			},
			onClose: function() {
				this.poDialog._oDialog.close();
			}
		}
	});
});