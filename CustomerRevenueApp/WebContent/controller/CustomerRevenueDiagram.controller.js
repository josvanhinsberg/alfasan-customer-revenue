jQuery.sap.require("sap.viz.ui5.controls.VizFrame");
jQuery.sap.require("sap.viz.ui5.data.FlattenedDataset");

sap.ui.define([
	"../utils/BaseController",
	"../model/ODataModel",
	"sap/ui/model/json/JSONModel"
], function(BaseController, ODataModel, JSONModel) {
	"use strict";
	return BaseController.extend("nl.alfasan.customerrevenue.controller.CustomerRevenueDiagram", {
		onInit: function() {
			this._getRouter().attachRouteMatched(this.onRouteMatched, this);
		},
		onRouteMatched: function(e) {
			if (e.getParameter("name") != "Customer") {
				return;
			}
			var id = e.getParameter("arguments").id || false,
			aFilter = [],
			mFilter = new sap.ui.model.Filter(
				"Kunnr", 
				sap.ui.model.FilterOperator.EQ,
				id
			);
			if(id != false){
				aFilter.push(mFilter);
				this._createDiagramModel(aFilter);
			}
		},
		_createDiagramModel: function(filter) {
			//Set the filter value for the OData call
			var mTotal	  	= {},
				oVizFrame 	= this.getView().byId("idVizFrame");
			
			//Perform the OData call
			var mSaleTotals = ODataModel.read("/RevenueTableSet", {filters: filter, success : function(mSaleTotals){ 
					//Calculate the totals per year
					var sales0 = 0,
						sales1 = 0,
						sales2 = 0,
						sales3 = 0;
					for (var i in mSaleTotals.results){
						sales0 += Number(mSaleTotals.results[i].Netwr0);
						sales1 += Number(mSaleTotals.results[i].Netwr1);
						sales2 += Number(mSaleTotals.results[i].Netwr2);
						sales3 += Number(mSaleTotals.results[i].Netwr3);
					}
					//Prepare the total year revenue table
					var year0 = new Date().getFullYear(),
						year1 = new Date().getFullYear() - 1,
						year2 = new Date().getFullYear() - 2,
						year3 = new Date().getFullYear() - 3;
					mTotal[0] = { year: year0, revenue: sales0 };
					mTotal[1] = { year: year1, revenue: sales1 };
					mTotal[2] = { year: year2, revenue: sales2 };
					mTotal[3] = { year: year3, revenue: sales3 };
	
					//Ensure the Diagram is cleared before showing new data
					oVizFrame.destroyFeeds();
		            oVizFrame.destroyDataset();
		            //Map the data 
		            var aChartData =  $.map(mTotal, function(value) {
		                return [value];
		            });
		            
		            //Store the data in a JSONModel
		            var oModel = new sap.ui.model.json.JSONModel({
		            	'data' : aChartData
		            });
		            
		            //Prepare the dataset for the diagram
		            var oDataSet = new sap.viz.ui5.data.FlattenedDataset({
		            	dimensions : [{
		            		name: 'Year',
		            		value: "{year}"
		            	}],
		            	measures : [{
		            		name: 'Revenue',
		            		value: "{revenue}"
		            	}],
		            	data : {
		            		path : "/data"
		            	}
		            });
		            
		            //Prepare the axis of the diagram
		            var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
			                'uid': "valueAxis",
			                'type': "Measure",
			                'values': ["Revenue"]
		            	}),
		            	feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
			                'uid': "categoryAxis",
			                'type': "Dimension",
			                'values': ["Year"]
		            	});
		            
		            //Provide the Diagram all the necessary data references
		            oVizFrame.setDataset(oDataSet);
		            oVizFrame.setModel(oModel);
		            
		            //Set the axis of the diagram
		            oVizFrame.addFeed(feedValueAxis);
		            oVizFrame.addFeed(feedCategoryAxis);
		            oVizFrame.setVizProperties({
		            	title: {
		            		visible: false,
		            		text:	 "Revenue of last 3 years"
		            	}
		            });
				} 
			});
			
			
            
		}
	});
	
});