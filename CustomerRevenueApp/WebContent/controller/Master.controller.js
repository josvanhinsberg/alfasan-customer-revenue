sap.ui.define([
	"../utils/BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/ODataModel"
], function(BaseController, JSONModel, ODataModel) {
	"use strict";
	return BaseController.extend("nl.alfasan.customerrevenue.controller.Master",{
		
		//Initialization
		onInit: function() {
			var fFilterInit = $.proxy(this.filter.onInit, this);	
			fFilterInit();	
			this._getRouter().attachRouteMatched(this.onRouteMatched, this);	
		},
		
		//Variable for preventing reloading the filter on startup
		_bPreventStepDialog: false,
		
		//Start the filter action on start of the app
		onAfterRendering: function() {

		},
		
		onRouteMatched: function(e) {
			// prevent the popup to show when accessing the client by URL 
			// and show all customers
			if (! this._bPreventStepDialog && e.getParameter("name") == "Master") {
				this._bPreventStepDialog = true;
				var fnSelectDialog = $.proxy(this.filter.organizations.openDialog, this);
				fnSelectDialog(true);
			}	

		},
		
		onSelect: function(e){
			var oCtx = e.getSource().getBindingContext("ODataModel");
			//Start the navigation to the Customer screen
			this._navTo("Customer", { id: oCtx.getProperty("Kunnr")} );
		},
		
			
		//Filter options
		filter: {
			_oDialogCust: null,
			_oDialogOrg:  null,
			_oDialogProd: null,
			
			onInit: function() {
			},
			
			onCancel : function(){
//				this.filter._oDialog.destroy(true);
//				this.filter._oDialog = null;
			},
			
			masterList: function() {
				var oBinding = this.getView().byId("Customers").getBinding("items");
				oBinding.filter(this.filter.customerRevenueList._aFilter);
			},
			
			//Customers filter functionality
			customers: {
				_aFilter: [],
				openDialog: function(bSteps) {
					// Create the dialog
					var sFragment = "nl.alfasan.customerrevenue.view.filterDialogCustomers";
					if(!this.filter._oDialogCust){
						this.filter._oDialogCust = sap.ui.xmlfragment(sFragment, sFragment, this);
					}
									
					//Set the dialog Model
					this.filter._oDialogCust.setModel(new JSONModel({
						bSteps: bSteps
					}),
					"Dialog");
					
					// Add dependant so we can use the i18n model inside the dialog
					this.getView().addDependent(this.filter._oDialogCust);
					// Open the Dialog
				    this.filter._oDialogCust.open();
					
			        //Link the fragment to its ID
					var oCustomerFilter = sap.ui.getCore().byId(sap.ui.core.Fragment.createId(sFragment , "CustomerFilter"));
					//Apply the filter values
					oCustomerFilter.getBinding("items").filter(this.filter.customers._aFilter);
				},
				
				onSearch: function(oEvent) {
					var sValue = oEvent.getParameter("value");
					var oFilter = new sap.ui.model.Filter("Name1", sap.ui.model.FilterOperator.Contains, sValue);
					var oBinding = oEvent.getSource().getBinding("items");
					oBinding.filter([oFilter]);
				},
				
				onConfirm : function(e) {
					var aSelectedItemIds	= [],
					mSelectedItems		= e.getParameter("selectedItems");
					if (mSelectedItems.length) {
						this.filter.products._aFilter = [];
						for(var i in mSelectedItems) {
							var	oCntx		= mSelectedItems[i].getBindingContext("ODataModel"),
								sPath		= oCntx.getPath(),
								mModel		= oCntx.getModel().getProperty(sPath);

							//Build filter values for the Products
							var mFilter = new sap.ui.model.Filter(
									"Matnr", 
									sap.ui.model.FilterOperator.EQ,
									mModel.Kunnr
								);
							this.filter.products._aFilter.push(mFilter);
							var mFilterRev = new sap.ui.model.Filter(
									"Kunnr", 
									sap.ui.model.FilterOperator.EQ,
									mModel.Kunnr
								);
							this.filter.customerRevenueList._aFilter.push(mFilterRev);
						}
						
						//Move on to the next screen
						if (this.filter._oDialogCust.getModel("Dialog").getProperty("/bSteps")) {
							// open step 3
							var fnStep3 = $.proxy(this.filter.products.openDialog, this);
							fnStep3(true);
						}
					} else
					{
						sap.ui.define(["sap/m/MessageBox"], function(MessageBox) {
							MessageBox.error("Please select at least one Customer");
						});
					}	
					
				}
			},
			
			//Products filter functionality
			products: {
				_aFilter: [],
				openDialog: function(bSteps) {
					// Create the dialog
					var sFragment = "nl.alfasan.customerrevenue.view.filterDialogProductsNew";
					if(!this.filter._oDialogProd){
						this.filter._oDialogProd = sap.ui.xmlfragment(sFragment, sFragment, this);
					}
					//Set the dialog Model
					this.filter._oDialogProd.setModel(new JSONModel({
						bSteps: bSteps
					}),
					"Dialog");

					// Add dependant so we can use the i18n model inside the dialog
					this.getView().addDependent(this.filter._oDialogProd);
					// Open the Dialog
					this.filter._oDialogProd.open();
					
					//Link the fragment to its ID
					var oProductFilter = sap.ui.getCore().byId(sap.ui.core.Fragment.createId(sFragment , "ProductFilter"));
					//Apply the filter values
					oProductFilter.getBinding("items").filter(this.filter.products._aFilter);
				},
				onSearch: function(oEvent) {
					var sValue = oEvent.getParameter("value");
					var oFilter = new sap.ui.model.Filter("Maktx", sap.ui.model.FilterOperator.Contains, sValue);
					var oBinding = oEvent.getSource().getBinding("items");
					var aFilter = [];
					aFilter.push(oFilter);
					//Take the original filter also into account
					if (this.filter.products._aFilter.length){
						for(var j in this.filter.products._aFilter){
							oFilter = this.filter.products._aFilter[j];
							aFilter.push(oFilter);
						}
					}
					oBinding.filter(aFilter);
				},
				onConfirm : function(e) {
					var aSelectedItemIds	= [],
					mSelectedItems		= e.getParameter("selectedItems");
					if (mSelectedItems.length) {
						
						for(var i in mSelectedItems) {
							var	oCntx		= mSelectedItems[i].getBindingContext("ODataModel"),
								sPath		= oCntx.getPath(),
								mModel		= oCntx.getModel().getProperty(sPath);

							//Build filter values for the Masterlist
							var mFilter = new sap.ui.model.Filter(
									"Matnr", 
									sap.ui.model.FilterOperator.EQ,
									mModel.Matnr
								);
							this.filter.customerRevenueList._aFilter.push(mFilter);
						}
					}
					//Build the Customer Revenue list
					var fnMasterList = $.proxy(this.filter.masterList, this);
					fnMasterList();
				},
				selectAll: function(e){
					var table = this.byId("ProductFilter");
					table.getItems().forEach( function(item) {
						var oCheckbox = item.getCells()[0];
						oCheckbox.setSelected(true);
					})
				}
			},
			
			//Organizations filter functionality
			organizations: {
				_aFilter: [],
				openDialog: function(bSteps) {
					// Create the dialog
					var sFragment = "nl.alfasan.customerrevenue.view.filterDialogOrganizations";
					if(!this.filter._oDialogOrg){
						this.filter._oDialogOrg = sap.ui.xmlfragment(sFragment, sFragment, this);
					}
					//Set the dialog Model
					this.filter._oDialogOrg.setModel(new JSONModel({
						bSteps: bSteps
					}),
					"Dialog");

					// Add dependant so we can use the i18n model inside the dialog
					this.getView().addDependent(this.filter._oDialogOrg);
					// Open the Dialog
					this.filter._oDialogOrg.open();
				},
				
				onConfirm : function(e) {
					var aSelectedItemIds	= [],
					mSelectedItems		= e.getParameter("selectedItems");
					if (mSelectedItems.length) {
						this.filter.customers._aFilter = [];
						for(var i in mSelectedItems) {
							var	oCntx		= mSelectedItems[i].getBindingContext("ODataModel"),
								sPath		= oCntx.getPath(),
								mModel		= oCntx.getModel().getProperty(sPath);
							//Build filter values for the Customers
							var mFilter = new sap.ui.model.Filter(
									"Vkorg", 
									sap.ui.model.FilterOperator.EQ,
									mModel.Vkorg
								);
							this.filter.customers._aFilter.push(mFilter);
						}
						
						//Move on to the next screen
						if (this.filter._oDialogOrg.getModel("Dialog").getProperty("/bSteps")) {
							// open step 2
							var fnStep2 = $.proxy(this.filter.customers.openDialog, this);
							fnStep2(true);
						}
					} else
					{
						sap.ui.define(["sap/m/MessageBox"], function(MessageBox) {
							MessageBox.error( "Please select at least one Sales Organization" );
						});
					}	
					
				}
			},
			
			//Customer Revenue list 
			customerRevenueList: {
				_aFilter: []
			}
		}
	});
});