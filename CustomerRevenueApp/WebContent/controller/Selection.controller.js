sap.ui.define([
	"nl/alfasan/customerrevenue/utils/BaseController"
], function(BaseController) {
	"use strict";
	return BaseController.extend("nl.alfasan.customerrevenue.controller.Selection", {
		onProceed: function() {
			this._navTo();
		}
	});
});