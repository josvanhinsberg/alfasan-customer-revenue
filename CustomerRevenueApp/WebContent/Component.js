sap.ui.define([
	"sap/ui/core/UIComponent",
	"./model/ODataModel",
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(UIComponent, ODataModel, JSONModel, Device){
	return UIComponent.extend("nl.alfasan.customerrevenue.Component", {
		// Component Uses manifest.json in the root folder
		metadata: { 
			manifest: "json" 
		},
		
		/**
		 * Initialize Component
		 */
		init: function() {
			//window.location = "#";
			// call parent
			UIComponent.prototype.init.apply(this, arguments);
			
			// set the device model
			var oDeviceModel = new JSONModel(Device);
			oDeviceModel.setDefaultBindingMode("OneWay");
			this.setModel(oDeviceModel, "device");
			
			this.setModel( ODataModel, "ODataModel" );
			// set and load modes
			var that = this;
			ODataModel.attachMetadataLoaded( function (){
				that.getRouter().initialize();
			});
			
			this._setModels();
			// Create the view based on the URL Hash (router set in manifest.json)
			
		},

		/**
		 * Set and Load the oData model
		 */
		_setModels: function() {
			//i18n translations
			this.setModel(
				new sap.ui.model.resource.ResourceModel({
					bundleName:'nl.alfasan.customerrevenue.i18n.i18n'
				}),
				'i18n'
			);
		}
	});
});