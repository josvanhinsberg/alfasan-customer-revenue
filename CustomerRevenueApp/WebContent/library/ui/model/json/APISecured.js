(function($, windows, undefined){
	jQuery.sap.require("jquery.sap.storage");
	jQuery.sap.declare('com.barrydam.sap.ui.model.json.APISecured'); // unresolved dependency added by SAPUI5 'AllInOne' Builder
	sap.ui.define(
		'com/barrydam/sap/ui/model/json/APISecured',
		['sap/ui/base/Object', 'sap/ui/model/json/JSONModel'],
		function(BaseObject, JSONModel) {
			"use strict";

			var mDefaultParameters = {
				apiActionKey	: '_action',
				primaryKey		: 'id',
				serviceBaseUrl	: '',
				type			: 'POST', // the type of request to make (POST OR GET)
				clientId		: 0,
				localStorageId	: false,
				useLocalStorage : true, // store or not
				onExpiredSession	: function(){
					alert('Your Server Session has expired, please restart the app and login');
				},
				onLoggedIn		: null,
				onLoggedOut		: null
				//,
				//clientSecret
			};

			var _static = {};

			/**
			 * Creates getters and setters for the allowed to change params of the _mSettings object
			 * This method is only called once by the API constructor
			 * @example primaryKey will generate getPrimaryKey and setPrimaryKey
			 *  
			 * @param  object  API the created com.barrydam.sap.ui.model.json.API object
			 */
			_static.createSettersAndGetters = function(API) {
				$.each(mDefaultParameters, function(key) { // $.each used in stead of for! else key would be allways the last iteration
					var f		= key.charAt(0).toUpperCase(),
						sSetter	= 'set'+f+key.substr(1),
						sGetter = 'get'+f+key.substr(1);
					API[sSetter] = function(value) {
						_static._set(API, key, value);						
					};
					API[sGetter] = function() {
						return API._mSettings[key];
					};
				});
			};

			/**
			 * used in constructor and createSettersAndGetters,
			 * magic _set method 
			 */
			_static._set = function(API, key, value) {
				switch (key) {
					// determine the service base url and its parameters
					case 'serviceBaseUrl' :
						var aUrl = value.split('?');
						if (aUrl.length > 1) {
							value = aUrl[0];
							if (aUrl[1]) {
								var aUrlParameters = aUrl[1].split('&');
								for (var keyUrlParameter in aUrlParameters) {
									var aUrlParameter = aUrlParameters[keyUrlParameter].split('=');
									API._mSettings.mUrlParameters[aUrlParameter[0]] = aUrlParameter[1] || false;
								}
							}
						}
						break;
				}
				// store in settings
				API._mSettings[key] = value;
			};

			/**
			 * NOTE: the ajax is called sync .. ! this is important for the oauth process
			 * this method is only for private useage
			 *
			 * the error callback first param is a boolean
			 * true = the error is thrown by the api itself 
			 * false = the error is caused by a ajax error
			 */
			_static.APIRequest = function (APISecured, sURL, sAction, mData, success, error, bSkipRefreshToken) {
				if (! APISecured)
					return;
				if (typeof mData !== 'object')
					mData = {};				
				// prepare data to send
				var mMerge = {
					client_id : APISecured.getClientId()
				};
				// add access token
				if (sAction !== 'login' && sAction !== 'passwordreset') {
					mMerge._token = APISecured.getOAuthAccessToken();
				}				
				var mSendData = $.extend(true, {}, mData, APISecured._mSettings.mUrlParameters, mMerge);
				// add action
				if (sAction)
					mSendData[APISecured.getApiActionKey()] = sAction;
				// POST sends booleans as string (false will be "false")
				// therefore reset them to an empty string
				if (APISecured.getType() == 'POST') {
					for (var key in mSendData)
						if (! mSendData[key])
							mSendData[key] = '';
				}

				// private function only needed in this scope
				function sessionExpired() {
					jQuery.sap.log.info('Access denied or Session Expired');
					APISecured.setOAuthAccessToken(false);
					APISecured.setOAuthRefreshToken(false);
					APISecured._CurrentUser = null;
					if (typeof APISecured.getOnExpiredSession() === 'function')
						APISecured.getOnExpiredSession()();
				}
				// do the synchronized ajax call
				return $.ajax.call(APISecured, {
					type     : 'POST',
					url      : sURL,
					data     : mSendData,
					dataType : 'json',
					cache	: false,
					async    : (['login', 'logout', 'refresh', 'passwordreset'].indexOf(sAction) === -1),
					success : function(mResponse) {
						if (mResponse.success) {
							if (typeof success === 'function')
								success(mResponse);
							return;
						} 
						var sOAuthError = mResponse.oauth || false;
						if (sOAuthError == 'access_denied') {
							sessionExpired();
							return;
						} else if (sOAuthError == 'access_token_expired' && ! bSkipRefreshToken) {
							jQuery.sap.log.info('Token expired, reset is initiated');
							APISecured.refreshToken(
								function onSucces() {
									jQuery.sap.log.info('Token is resetted, the request is now re-send');
									// token is resetted so try again
									_static.APIRequest(APISecured, sURL, sAction, mData, success, error, true);
								},
								function onError(mError) {
									var sOAuthError = mError.oauth || false;
									if (sOAuthError == 'access_denied' || sOAuthError == 'refresh_token_invalid') {
										sessionExpired();
									} else if (typeof error === 'function') {
										error(mError);
									} else {
										jQuery.sap.log.info('an error had occured', mError);
									}
								}
							);
						} else if (typeof error === 'function') {
							error(true, mResponse);
						}						 
					}, 
					error:	function(XMLHttpReq, txtStatus, errorThrown) {
						if (txtStatus !== 'abort') {
							if (typeof error === 'function')
								error(false, { error: XMLHttpReq.responseText });
							jQuery.sap.log.error("The following problem occurred: "+ txtStatus+', '+XMLHttpReq.responseText+', ' + XMLHttpReq.status + ',' + XMLHttpReq.statusText);
						}
					}
				});

			};
			
			_static.setRefreshTokenTimeout = function(APISecured, nSeconds) {
				var miliseconds = (nSeconds) ? parseInt(nSeconds, 10)*1000 :  3600000 ; // default one hour
				miliseconds = miliseconds - 10000; // min 10 sec
				if (APISecured._refreshTimeout !== null) {
					clearTimeout(APISecured._refreshTimeout);
					APISecured._refreshTimeout = null;
				}
				APISecured._refreshTimeout = setTimeout(
					function(){
						APISecured.refreshToken();
					}, 
					miliseconds
				);				
			};

			// base64 enc
			_static.Base64 = {
				_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
				encode: function(input) {
					var output = "", chr1, chr2, chr3, enc1, enc2, enc3, enc4, i = 0;
					input = _static.Base64._utf8_encode(input);
					while (i < input.length) {
						chr1 = input.charCodeAt(i++);
						chr2 = input.charCodeAt(i++);
						chr3 = input.charCodeAt(i++);
						enc1 = chr1 >> 2;
						enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
						enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
						enc4 = chr3 & 63;
						if (isNaN(chr2)) {
							enc3 = enc4 = 64;
						} else if (isNaN(chr3)) {
							enc4 = 64;
						}
						output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
					}
					return output;
				},
				decode: function(input) {
					var output = "", chr1, chr2, chr3, enc1, enc2, enc3, enc4, i = 0;
					input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
					while (i < input.length) {
						enc1 = this._keyStr.indexOf(input.charAt(i++));
						enc2 = this._keyStr.indexOf(input.charAt(i++));
						enc3 = this._keyStr.indexOf(input.charAt(i++));
						enc4 = this._keyStr.indexOf(input.charAt(i++));
						chr1 = (enc1 << 2) | (enc2 >> 4);
						chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
						chr3 = ((enc3 & 3) << 6) | enc4;
						output = output + String.fromCharCode(chr1);
						if (enc3 != 64) { output = output + String.fromCharCode(chr2); }
						if (enc4 != 64) { output = output + String.fromCharCode(chr3); }
					}
					return _static.Base64._utf8_decode(output);
				},
				_utf8_encode: function(string) {
					string = string.replace(/\r\n/g, "\n");
					var utftext = "";
					for (var n = 0; n < string.length; n++) {
						var c = string.charCodeAt(n);
						if (c < 128) {
							utftext += String.fromCharCode(c);
						} else if ((c > 127) && (c < 2048)) {
							utftext += String.fromCharCode((c >> 6) | 192);
							utftext += String.fromCharCode((c & 63) | 128);
						} else {
							utftext += String.fromCharCode((c >> 12) | 224);
							utftext += String.fromCharCode(((c >> 6) & 63) | 128);
							utftext += String.fromCharCode((c & 63) | 128);
						}
					}
					return utftext;
				},
				_utf8_decode: function(utftext) {
					var string = "", i = 0, c = 0, c1 = 0, c2 = 0;
					while (i < utftext.length) {
						c = utftext.charCodeAt(i);
						if (c < 128) {
							string += String.fromCharCode(c);
							i++;
						} else if ((c > 191) && (c < 224)) {
							c2 = utftext.charCodeAt(i + 1);
							string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
							i += 2;
						} else {
							c2 = utftext.charCodeAt(i + 1);
							c3 = utftext.charCodeAt(i + 2);
							string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
							i += 3;
						}
					}
					return string;
				}
			};
			
			_static.saveOAuthToLocalStorage = function(APISecured) {
				if (! APISecured.getUseLocalStorage())
					return;
				if (! jQuery.sap.storage.isSupported())
					return;
				var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
				if (APISecured.getOAuthRefreshToken()) {
					oStorage.put(APISecured.getLocalStorageId(), {
						reset: _static.Base64.encode(APISecured.getOAuthRefreshToken())
					});
				} else {
					oStorage.remove(APISecured.getLocalStorageId());
				}
			};

			_static.loadOAuthFromLocalStorage = function(APISecured) {
				var onError = function() {
					if (typeof APISecured.getOnLoggedOut() === 'function') {
						APISecured.getOnLoggedOut().call(APISecured);
						APISecured.trigger('logout');
					}
				};
				if (! APISecured.getUseLocalStorage()) {
					onError();
					return;
				}
				if (! jQuery.sap.storage.isSupported()) {
					onError();
					return;
				}
				var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
				var oOauth = oStorage.get(APISecured.getLocalStorageId());
				if (typeof oOauth !== 'object' || oOauth === null) {
					onError();
					return;
				}
				var sRefreshToken = oOauth.reset || false;
				if (! sRefreshToken) {
					onError();
					return;
				}				
				APISecured.setOAuthRefreshToken(_static.Base64.decode(sRefreshToken), true);			
				APISecured.refreshToken(
					function onSuccess(mResponse) {
						var mUser = mResponse.User || null;
						_static.setUserByAPIresponse(APISecured, mUser);
						if (APISecured.getUserModel() && typeof APISecured.getOnLoggedIn() === 'function') {
							APISecured.getOnLoggedIn().call(APISecured, APISecured.getUserModel());
							APISecured.trigger('login');
						} else {
							onError();
						}
					},
					onError, // no error 
					{
						getUser : true
					}
				);
			};

			_static.setUserByAPIresponse = function(APISecured, mUser) {
				if (typeof mUser !== 'object')
					return;
				// set and return the current user
				var mParameters = {
					customPostMethod : function(url, data, callback, datatype) {
						return _static.APIRequest(
							APISecured, 
							url,
							'', 
							data,
							function onSucces() {						
								if (typeof callback === 'function') {
									callback.apply(APISecured, arguments);
								}
							//	that.trigger(sModel);
							},
							function error(bApiError, mError) {
								if (typeof callback === 'function')
									callback(mError);
							}
						);
					}
				};
				var mNew = new com.barrydam.sap.ui.model.json.API(
					mUser.serviceURL,
					mParameters,
					true // do not send request on creation
				);
			//	mNew._mSettings.mUrlParameters = APISecured._mSettings.mUrlParameters;
				mNew._mSettings.mUrlParameters['_filterIds'] = [mUser.model.id];
				var mModel = {};
				mModel[mUser.model.id] = mUser.model;
				mNew.setData(mModel);
				APISecured._CurrentUser = mNew.getOne(mUser.model.id);
			};


			/**
			 * APISecured
			 */
			var APISecured = BaseObject.extend(
				'com.barrydam.sap.ui.model.json.APISecured',
				{
					
					/**
					 * holds the oAuth tokens
					 */
					_oauth: {
						accessToken: false,
						refreshToken: false
					},

					/**
					 * settings object is set in the constructor and is a merge of 
					 * - mDefaultParameters (defined within this scope)
					 * - mParameters (passed in constructor)
					 * - and settings
					 * @type {Object}
					 */
					_mSettings : {
						mUrlParameters : {} // holds the url parameters
					},

					// holds the refreshtimeout
					_refreshTimeout: null,

					// holds the current user (set at login)
					// holds a getOne from com.barrydam.nl.sap.ui.model.json.API
					_CurrentUser: null,

					constructor: function(sServiceBaseURL, mParameters) {
						BaseObject.apply(this); // do not pass arguments
						// create setters and getters on object creation
						_static.createSettersAndGetters(this);
						// the constructor can also be used in this way:
						// function(mParamters)
						// check which one is used
						if (typeof sServiceBaseURL === 'object') {
							mParameters = sServiceBaseURL;
						} else {
							this.setServiceBaseUrl(sServiceBaseURL);
							if (typeof mParameters !== 'object')
								mParameters = {};
						}
						// Set the settings and check if passed param is allowed to set
						// any passed parameter which is not in the mDefaultParameters
						// will not be stored			
						var	aDefaultParametersKeys	= Object.keys(mDefaultParameters),
							onFirstLoadSuccess, onFirstError;
						for (var kParameter in mParameters) {
							if (['onExpiredSession', 'onLoggedIn', 'onLoggedOut'].indexOf(kParameter) !== -1) {
								this._mSettings[kParameter] = mParameters[kParameter];
							} else if (aDefaultParametersKeys.indexOf(kParameter) !== -1 && typeof mParameters[kParameter] === typeof mDefaultParameters[kParameter]) {
								_static._set(this, kParameter, mParameters[kParameter]);
							}
						}
						// merge the mDefaultSettings with the _mSettings to make sure whe have every needed param
						this._mSettings	= $.extend(true, {}, mDefaultParameters, this._mSettings);
						// function in local scope
						function generateHash(str) {
							var hash = 0, i, chr, len;
							if (str.length === 0) 
								return hash;
							for (i = 0, len = str.length; i < len; i++) {
								chr   = str.charCodeAt(i);
								hash  = ((hash << 5) - hash) + chr;
								hash |= 0; // Convert to 32bit integer
							}
							return hash;
						}
						// create local storage id
						if (! this.getLocalStorageId()) {
							this._mSettings.localStorageId = 'APISecured_'+generateHash(this.getServiceBaseUrl()+this.getClientId()); 
						}
						// try to get oauth from local storage
						_static.loadOAuthFromLocalStorage(this);
					}
				}
			);
	
			APISecured.prototype.getOAuthAccessToken = function() {
				return this._oauth.accessToken;
			};
			APISecured.prototype.setOAuthAccessToken = function(sToken) {
				this._oauth.accessToken = sToken;
			};
			APISecured.prototype.getOAuthRefreshToken = function() {
				return this._oauth.refreshToken;
			};
			APISecured.prototype.setOAuthRefreshToken = function(sToken, bDisableSaveToLocalStorage) {
				this._oauth.refreshToken = sToken;
				if (! bDisableSaveToLocalStorage)
					_static.saveOAuthToLocalStorage(this);
			};
			
			/**
			 * [login description]
			 * mParameters.success
			 * 		object com.barrydam.sap.ui.model.json.API getOne of the user
			 * mParameters.error 
			 * 		string type invalid || error (invalid means the user credentials are incorrect)
			 * 		string error
			 * 
			 */
			APISecured.prototype.login = function(sUsername, sPassword, mParameters) {
				var mData = {},
					mSendData = {};
				if (typeof sUsername == 'object') {
					mSendData   = sUsername;
					mParameters = sPassword;
					if (! ('username' in mSendData) || ! ('password' in mSendData)) {
						if (typeof fOnError == 'function')
							fOnError('invalid', '');
						return;
					}
				} else {
					mSendData = {
						username: sUsername,
						password: sPassword
					};
				}
				if (typeof mParameters != 'object')
					mParameters = {};
				var fOnSuccess	= mParameters.success || false,
					fOnError	= mParameters.error || false;
				if (! mSendData.username || ! mSendData.password) {
					if (typeof fOnError == 'function')
						fOnError('invalid', '');
					return;
				}
				mData[this.getApiActionKey()] = 'login';
				var that = this;
				_static.APIRequest(
					this, // proxy
					this.getServiceBaseUrl(),
					'login',
					mSendData,
					function onSuccess(mData) {
						// oauth data
						_static.setRefreshTokenTimeout(that, mData.oauth.expires_in);
						that.setOAuthAccessToken(mData.oauth.access_token);
						that.setOAuthRefreshToken(mData.oauth.refresh_token);
						var mUser = mData.User || null;
						_static.setUserByAPIresponse(that, mUser);
						if (typeof fOnSuccess === 'function') {
							fOnSuccess(that.getUserModel());	
						} else if(typeof that.getOnLoggedIn() === 'function') {
							that.getOnLoggedIn().call(that, that.getUserModel());
						}
						// event trigger
						that.trigger('login');						
					},
					function onError(boolApiError, mData) {
						if (typeof fOnError !== 'function') 
							return;
						if (boolApiError) {
							var sOauth = mData.oauth || false;
							var mError	= mData.error || {},
								type = 'error';
							if (sOauth == 'invalid_credentials')
								type = 'credentials';
							if (sOauth == 'invalid_client')
								type = 'client';
							fOnError(type, mError);
						}
					}
				);
			};

			APISecured.prototype.logout = function(success, error) {
				var that = this;
				_static.APIRequest(
					this, // proxy
					this.getServiceBaseUrl(),
					'logout',
					{},
					function onSuccess() {
						// clear timeout
						if (that._refreshTimeout !== null)
							clearTimeout(that._refreshTimeout);
						that.setOAuthAccessToken(false);
						that.setOAuthRefreshToken(false);
						that._CurrentUser = null;
						if (typeof success === 'function') {
							success();
						} else if (typeof that.getOnLoggedOut() === 'function') {
							that.getOnLoggedOut().call(that);
						}
						that.trigger('logout');
					},
					function onError(boolApiError, mData) {
						if (typeof error === 'function')	
							error(mData.error);
					}
				);
			};

			APISecured.prototype.passwordReset = function(email, mParameters) {
				var mData = {};
				if (typeof mParameters != 'object')
					mParameters = {};
				var fOnSuccess	= mParameters.success || false,
					fOnError	= mParameters.error || false;				
				var that = this;
				_static.APIRequest(
					this, // proxy
					this.getServiceBaseUrl(),
					'passwordreset',
					{ email: email },
					function onSuccess(mData) {
						// oauth data
						if (typeof fOnSuccess === 'function') {
							fOnSuccess(that.getUserModel());	
						} else if(typeof that.getOnLoggedIn() === 'function') {
							that.getOnLoggedIn().call(that, that.getUserModel());
						}
						// event trigger
						that.trigger('passwordreset');						
					},
					function onError(boolApiError, mData) {
						if (typeof fOnError !== 'function') 
							return;
						if (boolApiError) {
							var sOauth = mData.oauth || false;
							var mError	= mData.error || {},
								type = 'error';							
							fOnError(type, mError);
						}
					}
				);
			};

			APISecured.prototype.refreshToken = function(success, error, mData) {
				if (this._refreshTimeout !== null) {
					clearTimeout(this._refreshTimeout);
					this._refreshTimeout = null;
				}
				var that		= this,
					mSendData	= { _refreshtoken: that.getOAuthRefreshToken() };
				if (typeof mData === 'object') {
					mSendData = $.extend(true, {}, mData, mSendData);
				}
				_static.APIRequest(
					this, 
					this.getServiceBaseUrl(),
					'refreshtoken', 
					mSendData,
					function onSuccess(mResponse) {
						_static.setRefreshTokenTimeout(that, mResponse.oauth.expires_in);
						that.setOAuthAccessToken(mResponse.oauth.access_token);
						that.setOAuthRefreshToken(mResponse.oauth.refresh_token);
						if (typeof success === 'function')
							success(mResponse);
					},
					function onError(bAPIError, mError) {
						if (typeof error === 'function')
							error(mError);
					},
					true
				);
			};

			APISecured.prototype.getUserModel = function() {
				return this._CurrentUser;
			};

			APISecured.prototype.getAPIModel = function(sModel, mParameters, bDisableFireSendOnConstruction) {
				if (typeof mParameters !== 'object')
					mParameters = {};
				var that = this;
				mParameters = $.extend(true, {}, mParameters);
				mParameters.customPostMethod = function(url, data, callback, datatype) {
					return _static.APIRequest(
						that, 
						url,
						'', 
						data,
						function onSucces() {						
							if (typeof callback === 'function') {
								callback.apply(that, arguments);
							}
							that.trigger(sModel);
						},
						function error(bApiError, mError) {
							if (typeof callback === 'function')
								callback(mError);
						}
					);
				};
				return new com.barrydam.sap.ui.model.json.API(
					this.getServiceBaseUrl()+sModel,
					mParameters,
					bDisableFireSendOnConstruction
				);
			};

			/**
			 * Events
			 * avalaible
			 * - logout
			 * - login
			 */
			APISecured.prototype.on = function(event, callback) {
				var that = this;
				if (event && typeof callback == 'function') {
					$(document).on('APISecured_'+event, function() {
						callback.apply(that, arguments);
					});
				}
			};

			APISecured.prototype.one = function(event, callback) {
				var that = this;
				if (event && typeof callback == 'function') {
					$(document).one('APISecured_'+event, function() {
						callback.apply(that, arguments);
					});
				}
			};

			APISecured.prototype.trigger = function(event) {
				$(document).trigger('APISecured_'+event);
			};

			APISecured.prototype.customCall = function(sAction, mData, mParameters) {				
				// loose reference
				mData = $.extend(true, {}, mData);
				mData[this._mSettings.apiActionKey] = '_customcall';
				mData._customAction = sAction;
				var fOnSuccess	= (mParameters) ? mParameters.success || false : false,
					fOnError	= (mParameters) ? mParameters.error || false : false,
					fOnAllways	= (mParameters) ? mParameters.allways || false : false;
				_static.APIRequest(
					this, // proxy
					this.getServiceBaseUrl()+'CustomCall',
					'customCall',
					mData,
					function onSuccess(mResponse) {
						if (mResponse.success) {
							if (typeof fOnSuccess == 'function')
								fOnSuccess(mResponse.response);
						} else {
							if (typeof fOnError == 'function')
								fOnError(mResponse);
						}						
						if (typeof fOnAllways == 'function')
							fOnAllways(mResponse.response);
					},
					function onError(bApiError, mError) {
						if (typeof fOnError == 'function')
							fOnError(mError.error);
						if (typeof fOnAllways == 'function')
							fOnAllways(mError);
					}
				);

			};

			return APISecured;

		},
		true // bExport
	);


})(jQuery, window);
