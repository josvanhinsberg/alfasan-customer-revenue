(function($, windows, undefined){
	jQuery.sap.declare('com.barrydam.sap.ui.model.json.JSONModel'); // unresolved dependency added by SAPUI5 'AllInOne' Builder
	sap.ui.define(
		'com/barrydam/sap/ui/model/json/JSONModel',
		['sap/ui/model/json/JSONModel'],
		function(sapUiJSONModel) {
			"use strict";

			var One = sapUiJSONModel.extend(
				'com.barrydam.sap.ui.model.json.JSONModelOne',
				{
					__JSONModel : null, // holds the com.barrydam.sap.ui.model.API object
					__id: false, // check if the APIOne is a new Model or existing
					/**
					 * Constructor fired on object creation
					 */
					constructor : function(mData, bdJSONModel, id) {
						mData				= $.extend(true, {}, mData);
						sapUiJSONModel.call(this, mData);
						this.__id			= id;
						this.__JSONModel	= bdJSONModel;
					}
				}
			);

			One.prototype.delete = function() {
				var m		= $.extend(true, {}, this.__JSONModel.getProperty('/')),
					mNew	= {};
				for (var i in m) {
					if (i != this.__id) {
						mNew[i] = m[i];
					}
				}
				this.__JSONModel.setProperty('/', mNew);
			};

			/**
			 * The setproperty will also set the parent API object
			 */
			One.prototype.setProperty = function(sPath, value, oContext) {
				// sPath = sPath.replace("/", "");
				// sapUiJSONModel.prototype.setProperty.apply(this, arguments);
				// var mModel = this.getProperty("/");	
				// this.__JSONModel.setProperty("/"+this.__id+"/"+sPath, value);
				sapUiJSONModel.prototype.setProperty.apply(this, arguments);
				var sResolvedPath	= this.resolve(sPath, oContext),
					mModel			= this.getProperty("/");	
				this.__JSONModel.setProperty("/"+this.__id+sResolvedPath, value);
			};

			One.prototype.getOne = function(id) {
				var m = this.getProperty('/');
				if (Object.keys(m).length === 0)
					return;
				if (id in m)
					return new One(m[id], this, id);					
			};	

			One.prototype.getNew = function(mDefaultValues) {
				var idNew = Date.now(),
					mData = (typeof mDefaultValues === 'object') ? mDefaultValues : {};
				this.setProperty('/'+idNew, mData);
				return this.getOne(idNew);			
			};	

			var bdJSONModel = sapUiJSONModel.extend(
				'com.barrydam.sap.ui.model.json.JSONModel',
				{
					constructor : function(mData) {
						mData				= $.extend(true, {}, mData);
						sapUiJSONModel.call(this, mData);
					}
				}
			);

			bdJSONModel.prototype.getOne = function(id) {
				var m = this.getProperty('/');
				if (Object.keys(m).length === 0)
					return;
				if (id in m)
					return new One(m[id], this, id);					
			};	

			bdJSONModel.prototype.getNew = function(mDefaultValues) {
				var mData = this.getProperty('/'),
					idNew = Date.now();
				mData[idNew] = (typeof mDefaultValues === 'object') ? mDefaultValues : {};
				this.setData(mData);
				return this.getOne(idNew);			
			};	

			return bdJSONModel;
		},
		true // bExport
	);
})(jQuery, window);