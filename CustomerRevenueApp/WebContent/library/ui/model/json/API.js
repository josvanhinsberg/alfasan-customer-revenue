/**
 * com.barrydam.sap.ui.model.json.API
 * @author	Barry Dam
 * @version 0.9.0
 * In your Component.js init method add:
 * jQuery.sap.registerModulePath('com.barrydam.sap.model.json', 'path to root folder of API.js');
 * jQuery.sap.require("com.barrydam.sap.ui.model.json.API");
 */
(function($, windows, undefined){
	jQuery.sap.declare('com.barrydam.sap.ui.model.json.API'); // unresolved dependency added by SAPUI5 'AllInOne' Builder
	sap.ui.define(
		'com/barrydam/sap/ui/model/json/API',
		['sap/ui/model/json/JSONModel'],
		function(JSONModel) {
			"use strict";

	/********************************************
	 *											*
	 * static functions for use in local scope	*
	 *											*
	 ********************************************/

			var _static = {};

			/**
			 * Check if the request can be executed immediately or stored for batch processing
			 * @param  object	API			the created com.barrydam.sap.ui.model.json.API object
			 * @param  object	mData		data to send
			 * @param  function fOnSuccess	function fired on send succes
			 * @param  functionfOnError		function fired on send error
			 */
			_static.batchOrSend = function(API, mData, fOnSuccess, fOnError, fOnAllways) {
				// in case of batch 
				// temp store it
				if (API.getUseBatch()) {
					API._aBatch.push({
						mData      : mData,
						fOnSuccess : fOnSuccess,
						fOnError   : fOnError,
						fOnAllways : fOnAllways
					});	
					// since we've placed the apicall in a batch
					// we need to manually modify the model
					var mApiData		= API.getData(),
						sAction			= mData[API.getApiActionKey()],
						aApiDataKeys	= Object.keys(mApiData);
					if (['delete', 'update'].indexOf(sAction) !== -1 && aApiDataKeys.length) {
						var sPrimaryKey		= API.getPrimaryKey(),
							primaryValue	= mData[sPrimaryKey],
							keyToWorkWith	= null;
						// first check if the key is equal to primarykey
						if (aApiDataKeys.indexOf(primaryValue) !== -1 && mApiData[primaryValue][sPrimaryKey] == primaryValue) {
							keyToWorkWith = primaryValue;
						} else { // else looop all data
							for (var key in mApiData) {
								if (mApiData[key][sPrimaryKey] == primaryValue) {
									keyToWorkWith = key;
									break;
								}
							}
						}
						if (keyToWorkWith===null)
							return;
						// exec deletion or update
						if (sAction == 'delete') {
							delete(mApiData[keyToWorkWith]);							
						} else if (sAction == 'update') {
							// only update allowed fields
							var aKeysAllowed = Object.keys(mApiData[keyToWorkWith]);
							for (var keyM in mData) {
								if (keyM == sPrimaryKey || aKeysAllowed.indexOf(keyM) === -1)
									continue;
								mApiData[keyToWorkWith][keyM] = mData[keyM];
							}
						}
						// re-set the data
						API.setData(mApiData);
					} else if (sAction == 'create') {
						//_static.log('_static.batchOrSend: batch > create not implemented yet');
					}
				} else {
					return API.send(mData, fOnSuccess, fOnError, fOnAllways);
				}
			};

			/**
			 * [processApiData description]
			 * @param  {[type]} API   [description]
			 * @param  {[type]} mData [description]
			 * @return object {
			 *	succes boolean	false on error
			 *	model	object	the model
			 * }
			 */
			_static.processApiData = function(mData) {
				var mReturn				= {};
				mReturn.success			= mData.success || false;
				mReturn.errorMessage	= mData.errorMessage || false;
				mReturn.columns			= mData.columns || {};
				mReturn.error			= mData.error || false;
				mReturn.allways			= mData.allways || false;
				delete mData.success;				
				// loose reference
				mReturn.model		= $.extend(true, {}, (mData.data || {}));
				return mReturn;
			};
			
			/**
			 * Creates getters and setters for the allowed to change params of the _mSettings object
			 * This method is only called once by the API constructor
			 * @example primaryKey will generate getPrimaryKey and setPrimaryKey
			 *  
			 * @param  object  API the created com.barrydam.sap.ui.model.json.API object
			 */
			_static.createSettersAndGetters = function(API) {
				$.each(mDefaultParameters, function(key) { // $.each used in stead of for! else key would be allways the last iteration
					var f		= key.charAt(0).toUpperCase(),
						sSetter	= 'set'+f+key.substr(1),
						sGetter = 'get'+f+key.substr(1);
					API[sSetter] = function(value) {
						_static._set(API, key, value);						
					};
					API[sGetter] = function() {
						return API._mSettings[key];
					};
				});
			};

			_static.createFilter = function(value) {
				if ($.isArray(value)) {
					var oFilter = {
						condition: [],
						and : ''
					};
					if ($.isArray(value[0])) {
						if (typeof value[1] != 'undefined') 
							oFilter.and = value[1] ? 1 : 0 ;
						for (var i in value[0])	{
							if (value[0][i].length !== 3) {
								_static.log('_set filter: invalid filter', value[0][i]);
								continue;
							}	
							oFilter.condition.push(
								{
									key      : value[0][i][0],
									operator : value[0][i][1],
									value    : value[0][i][2]
								}
							);
						}							
					} else if (value.length === 3){
						oFilter.condition.push(
							{
								key      : value[0],
								operator : value[1],
								value    : value[2]
							}
						);
					} else {
						_static.log('_set filter: invalid filter', value);
					}
					if (oFilter.condition.length) {
						return oFilter;
					}
				}
				return false;
			};

			/**
			 * used in constructor and createSettersAndGetters,
			 * magic _set method 
			 */
			_static._set = function(API, key, value) {
				switch (key) {
					case 'filter' :
						var oFilter = _static.createFilter(value);
						if (oFilter)
							API._mSettings.mUrlParameters._filter = oFilter;
						break;

					// determine the service base url and its parameters
					case 'serviceUrl' :
						var aUrl = value.split('?');
						if (aUrl.length > 1) {
							value = aUrl[0];
							if (aUrl[1]) {
								var aUrlParameters = aUrl[1].split('&');
								for (var keyUrlParameter in aUrlParameters) {
									var aUrlParameter = aUrlParameters[keyUrlParameter].split('=');
									API._mSettings.mUrlParameters[aUrlParameter[0]] = aUrlParameter[1] || false;
								}
							}
						}
						break;
					// Only POST and GET is allowed
					case 'type':
						value = value.toUpperCase();
						if (['POST', 'GET'].indexOf(value) === -1)
							value = mDefaultParameters.type;
						break;
					// only true or false
					case 'useBatch' :
						value = (value);
						break;

					case 'columns':
						if (API.getColumns().length !== 0)
							return;
						// return before break
						break;
					// possible more cases..
				}
				// store in settings
				API._mSettings[key] = value;
			};

			_static.log = function() {
				jQuery.sap.log.error('com.barrydam.sap.ui.model.json.API:', Array.prototype.join.call(arguments, ','));
			};

			/**
			 * custom postmethod = made for the APISecured integration
			 */
			_static.POST = function(API, url, data, success, datatype) {
				if (API.getCustomPostMethod() !== null) {
					return API.getCustomPostMethod().call(API, url, data, success, datatype);
				} else {
					return $.post.call(API,  url, data, success, datatype);	
				}
			};

	/********************************************
	 *											*
	 * APIOne									*
	 * only available within this scope			*
	 * used in getOne method of					*
	 * com.barrydam.sap.ui.model.json.API		*
	 *											*
	 ********************************************/

			/**
			 * Create the APIOne object (com.barrydam.sap.ui.model.json.APIOne)
			 * which is an extension of sap.ui.model.json.JSONModel
			 * with the following extra methods
			 * - APIUpdate
			 * - APISave
			 * - APIDelete
			 * - APIGetModel
			 */
			var APIOne = JSONModel.extend(
				'com.barrydam.sap.ui.model.json.APIOne',
				{
					__API : null, // holds the com.barrydam.sap.ui.model.API object
					__isNew: false, // check if the APIOne is a new Model or existing
					__resetData: null,
					/**
					 * Constructor fired on object creation
					 * @param  string sServiceUrl The URL to the JSON service
					 * @param  object mParameters overwrite settings for the mDefaultParameters value
					 */
					constructor : function(mData, API, bNew) {
						mData				= $.extend(true, {}, mData);
						JSONModel.call(this, mData);
						this.__resetData	= $.extend(true, {}, mData);
						this.__isNew		= bNew;
						this.__API			= API;
					}
				}
			);

			/**
			 * APIUpdate 
			 * @param mData object with update info
			 * @param mParameters (.success and .error) holds the succes and error functions
			 */
			APIOne.prototype.APIUpdate = function(mData, mParameters) {
				if (typeof mData !== 'object')
					return;
				mData = $.extend(true, {}, mData);
				if (this.__API.getPrimaryKey() in mData)
					delete(mData[primarykey]);
				var m = this.getProperty('/');
				for (var i in m) {
					if (i in mData)
						m[i] = mData[i];
				}
				this.setData(m);
				// save to api
				if (typeof mParameters != 'object')
					mParameters = {};
				var fOnSuccess	= mParameters.success || false,
					fOnError	= mParameters.error;
				this.APISave(fOnSuccess, fOnError);
			};

			/**
			 * mParameters
			 *		success
			 *		error
			 */
			APIOne.prototype.APISave = function(fOnsuccess, fOnError) {
				var that = this;
				if (this.__isNew) { // create a new entry
					this.__API.create(
						this.getProperty('/'),
						{
							success: function(mNewEntry) {
								that.__isNew = false;
								that.setData(mNewEntry);
								that.__resetData = $.extend(true, {}, mNewEntry);
								if (typeof fOnsuccess === 'function')
									fOnsuccess(mNewEntry);
							},
							error: fOnError
						}
					);
				} else { // update existing
					this.__API.update(
						this.getProperty('/'),
						{
							success : function() {
								that.__resetData = $.extend(true, {}, that.getProperty('/'));
								if (typeof fOnsuccess === 'function')
									fOnsuccess.apply(this, arguments);
							},
							error	: fOnError
						}
					);
				}
			};

			/**
			 * Delete this model from API
			 * @param function fOnsuccess success callback
			 * @param function fOnError   error callback
			 */
			APIOne.prototype.APIDelete = function(fOnsuccess, fOnError) {
				if (this.__isNew) {
					this.setData({});
					if (typeof fOnsuccess === 'function')
						fOnsuccess();
					return;
				}
				var mData = this.getProperty('/');
				if (! (this.__API.getPrimaryKey() in mData)) {
					if (typeof fOnError == 'function')
						fOnError();
					return;
				}
				this.__API.delete(
					mData[this.__API.getPrimaryKey()],
					{
						success : fOnsuccess,
						error	: fOnError
					}
				);
			};

			/**
			 * Get the Model From ORM .. the new model is returned in the first param of the succes callback method fOnsuccess
			 * @param string  sModelName	ORM Model name
			 * @param function fOnsuccess returns a new com.barrydam.ui.json.API on succes
			 * @param function fOnError   method trigerred on error
			 */
			APIOne.prototype.APIGetModel = function(sModelname, fOnsuccess, fOnError, fOnAllways) {
				if (this.__isNew) {
					return;
				}
				var mData	= {},
					that	= this;
				mData[this.__API.getApiActionKey()] = 'orm';
				mData.model							= sModelname;

				if (typeof fOnsuccess == 'object') {
					var mParameters = $.extend(true, {} ,fOnsuccess);
					fOnsuccess	= mParameters.success || false;
					fOnError	= mParameters.error || false;
					if ('filter' in mParameters) {
						mData._filter = _static.createFilter(mParameters['filter']);
					}
				}

				var m = this.getProperty('/');
				mData.key =	m[this.__API.getPrimaryKey()];
				var succesCallback	= fOnsuccess,
					errorCallback	= fOnError,
					allwaysCallback = fOnAllways;
				_static.POST(this.__API,
					this.__API.getServiceUrl(),
					mData,
					function(mResponse) {
						var mData				= {};
						mData.success			= mResponse.success || false;
						mData.errorMessage		= mResponse.errorMessage || false;
						mData.serviceURL		= mResponse.serviceURL || false;
						mData._filterIds		= mResponse._filterIds || false;
						mData.columns			= mResponse.columns || false;
						delete mResponse.success;				
						// lose reference
						mData.model		= $.extend(true, {}, (mResponse.data || {}));
						if (mData.success) {
						var aUrlNew = mData.serviceURL.split('/'),
							aUrlOld	= that.__API.getServiceUrl().split('/');
						aUrlOld[aUrlOld.length-1] = aUrlNew[aUrlNew.length-1];
						var sNewUrl = aUrlOld.join('/'),
							mAPIData = {
								customPostMethod : that.__API.getCustomPostMethod()
							};
							var mNew = new com.barrydam.sap.ui.model.json.API(sNewUrl, mAPIData, true);
							mNew._mSettings.mUrlParameters['_filterIds'] = mData._filterIds;
							mNew.setData(mData.model);
							if (mData.columns)
								mNew.setColumns(mData.columns);
							if (typeof succesCallback === 'function')
								succesCallback(mNew);
							return mNew;
						} else {
							if (typeof errorCallback === 'function') {
								var strErrorMsg = mData.errorMessage || that.__API._mSettings.defaulErrorMessage ;
								errorCallback(strErrorMsg);
							}
						}
						if (typeof allwaysCallback === 'function') {
							allwaysCallback();
						}
					},
					'json'
				).fail(function(XMLHttpReq, textStatus, errorThrown) {
					if (textStatus !== 'abort') {
						// log error
						jQuery.sap.log.fatal("The following problem occurred: " + textStatus, XMLHttpReq.responseText + "," + XMLHttpReq.status + "," + XMLHttpReq.statusText);
						if (typeof fOnError === 'function')
							fOnError(XMLHttpReq, textStatus, errorThrown);
						if (typeof allwaysCallback === 'function') {
							allwaysCallback();
						}
					}
				});
			};

			/**
			 * The setproperty will also set the parent API object
			 */
			APIOne.prototype.setProperty = function(sPath, value) {
				sPath = sPath.replace('/', '');
				var mModel = this.getProperty('/');
				if (! (sPath in mModel) && sPath != this.__API.getPrimaryKey())
					_static.log('setProperty: '+sPath+'is not a valid field of ', this);
				mModel[sPath] = value;
				this.setData(mModel);	
				this.__API.setProperty('/'+mModel[this.__API.getPrimaryKey()]+'/'+sPath, value);
			};

			/**
			 * Reset the model to its original state
			 */
			APIOne.prototype.reset = function() {
				for (var i in this.__resetData) {
					this.setProperty(i, this.__resetData[i]);
				}
			};			


	/********************************************
	 *											*
	 * API										*
	 * com.barrydam.sap.ui.model.json.API		*
	 *											*
	 ********************************************/

			/**
			 * Default parameters and paramters that can be set in the constructor 
			 * as will be available as a set and get method (more check createSettersAndGetters)
			 * of com.barrydam.sap.ui.model.json.API
			 */
			var mDefaultParameters = {
				apiActionKey		: '_action',
				primaryKey			: 'id',
				serviceUrl			: '',
				type				: 'POST', // the type of request to make (POST OR GET)
				useBatch			: false, // when true all requests will be sent in batch
				defaulErrorMessage	: 'Something went wrong.', // default text returned on error
				filter				: [], // filter
				columns				: [],
				customPostMethod	: null
			};

			/**
			 * Create the com.barrydam.sap.ui.model.json.API object
			 */
			var API = JSONModel.extend(
				'com.barrydam.sap.ui.model.json.API',
				{
					/**
					 * settings object is set in the constructor and is a merge of 
					 * - mDefaultParameters (defined within this scope)
					 * - mParameters (passed in constructor)
					 * - and settings
					 * @type {Object}
					 */
					_mSettings : {
						mUrlParameters : {} // holds the url parameters
					},
					/**
					 * Holds the batch when 
					 * _mSettings useBatch is set to true
					 */
					_aBatch : [],

					/**
					 *  holds the jQuery XHR objects in array when a service call is executed
					 */
					_jqXhr : {},

					/**
					 * Holds reference to the object
					 * @type {[type]}
					 */
					_belongsTo: null,
					
					/**
					 * Constructor fired on object creation
					 * @param  string sServiceUrl The URL to the JSON service
					 * @param  object mParameters overwrite settings for the mDefaultParameters value
					 */
					constructor : function(sServiceUrl, mParameters) {
						JSONModel.apply(this); // do not pass arguments
						// reset settings (needed for getOne method)
						this._mSettings = {
							mUrlParameters : {} // holds the url parameters
						};
						// create setters and getters on object creation
						_static.createSettersAndGetters(this);
						// the constructor can also be used in this way:
						// function(mParamters)
						// check which one is used
						if (typeof sServiceUrl === 'object') {
							mParameters = sServiceUrl;
						} else {
							this.setServiceUrl(sServiceUrl);
							if (typeof mParameters !== 'object')
								mParameters = {};
						}
						// Set the settings and check if passed param is allowed to set
						// any passed parameter which is not in the mDefaultParameters
						// will not be stored			
						var	aDefaultParametersKeys	= Object.keys(mDefaultParameters),
							onFirstLoadSuccess, onFirstLoadError;
						for (var kParameter in mParameters) {
							if (kParameter === 'onFirstLoadSuccess') {
								onFirstLoadSuccess = mParameters[kParameter];
							} else if (kParameter === 'onFirstLoadError') {
								onFirstLoadError = mParameters[kParameter];
							} else if (kParameter === 'customPostMethod') {
								this._mSettings[kParameter] = mParameters[kParameter];								
							} else if (aDefaultParametersKeys.indexOf(kParameter) !== -1 && typeof mParameters[kParameter] === typeof mDefaultParameters[kParameter]) {
								_static._set(this, kParameter, mParameters[kParameter]);
							}
						}
						// merge the mDefaultSettings with the _mSettings to make sure whe have every needed param
						this._mSettings	= $.extend(true, {}, mDefaultParameters, this._mSettings);
						// Fire the first send request to the service to collect the model data
						// exept if the 3rd constructor param = true
						if (! (typeof arguments[2] !== 'undefined' && arguments[2] === true))
							this.send({}, onFirstLoadSuccess, onFirstLoadError);
					}
					// ,

					
				}
			);
// setProperty: function(sPath, oValue, oContext, bAsyncUpdate) {
					// 	_static.log('setProperty: Cannot use this method, use Create Update or Delete');
					// }
			/**
			 * Returns a child object as APIOne object
			 */
			API.prototype.getOne = function(PrimaryKeyValue) {
				if (this._belongsTo !== null)
					return;
				var m = this.getProperty('/');
				if (Object.keys(m).length === 0)
					return;
				var primarykey	= this.getPrimaryKey(),
					mFound		= null;
				for (var i in m) {
					if ((primarykey in m[i]) === false)
						return;
					if (m[i][primarykey] == PrimaryKeyValue) {
						mFound = $.extend(true, {}, m[i]);
						break;
					}
				}
				if (mFound !== null)
					return new APIOne(mFound, this);
			};

			/**
			 * return a new empty json model with an APISave method
			 */
			API.prototype.getNew = function(mDefaultValues) {
				var aColumns	= this.getColumns(),
					mData		= {};
				if (aColumns.length) {
					for (var keyColumns in aColumns) {
						mData[aColumns[keyColumns]] = (typeof mDefaultValues === 'object' && aColumns[keyColumns] in mDefaultValues) ? mDefaultValues[aColumns[keyColumns]] : '';
					}
				}
				return new APIOne(mData, this, true);
			};

			/**
			 * Submit the batch
			 * @param  function fOnSuccess fired when all the batch is processed succesfully
			 * @param  function	fOnError   fired when one of the batch proccesses has an error
			 */
			API.prototype.submitBatch = function(fOnSuccess, fOnError, fOnAllways) {
				if (typeof fOnSuccess == 'object') {
					if ('error' in fOnSuccess && typeof fOnSuccess.error == 'function') {
						fOnError = fOnSuccess.error;
					}
					if ('allways' in fOnSuccess && typeof fOnSuccess.allways == 'function') {
						fOnError = fOnSuccess.error;
					}
					if ('success' in fOnSuccess && typeof fOnSuccess.success == 'function') {
						fOnSuccess = fOnSuccess.success;
					}
				}

				var that			= this,
					aBatchErrors	= arguments[3] || [],
					mBatch			= null;
				// get the first from batch
				for (var i in this._aBatch) {
					mBatch = this._aBatch[i];
					break;
				}
				// check if the batch has completed
				if (! this._mSettings.useBatch || mBatch === null) {
					if (aBatchErrors.length && typeof fOnError) {
						fOnError(aBatchErrors);
					} else if (typeof fOnSuccess === 'function') {
						fOnSuccess();
					} 
					if (typeof fOnAllways === 'function') {
						fOnAllways();
					}

					return;
				}
				// execute batch
				this.send(
					mBatch.mData,
					function onSucces(mData){
						// batch onSuccess
						if (typeof mBatch.fOnSuccess === 'function') {
							mBatch.fOnSuccess(mData);
						}
						// delete current batch
						delete(that._aBatch[i]);
						// execute next batch
						that.submitBatch(fOnSuccess, fOnError, fOnAllways, aBatchErrors);
					},
					function onError(XMLHttpReq, textStatus, errorThrown) {
						//batch onError
						if (typeof mBatch.fOnError === 'function') {
							mBatch.fOnError(XMLHttpReq, textStatus, errorThrown);
						}
						// add error to batch errors
						aBatchErrors.push({
							batch: $.extend(true, {}, mBatch),
							error: arguments
						});
						// delete current batch
						delete(that._aBatch[i]);
						// execute next batch
						that.submitBatch(fOnSuccess, fOnError, fOnAllways, aBatchErrors);
					},
					function onAllways() {
						if (typeof mBatch.fOnAllways === 'function') {
							mBatch.fOnAllways();
						}						
					},					
					(this._aBatch.length !== 1) // only update the last one of the batch
				);
				return;
			};
				
			/**
			 * sends the final request to the server
			 * @param  object	mData                data to pass
			 * @param  function fOnSuccess           fired when the request has succeeded
			 *                                       first param isthe complete object
			 * @param  function	fOnError             fired when the request has failed
			 */
			API.prototype.send = function(mData, fOnSuccess, fOnError, fOnAllways, bDisableModelUpdate) {
				if (typeof mData !== 'object')
					mData = {};				
				// prepare data to send
				var mSendData = $.extend(true, {}, mData, this._mSettings.mUrlParameters);
				// POST sends booleans as string (false will be "false")
				// therefore reset them to an empty string
				// also emty arrays will not be send .. so reset them here too
				if (this.getType() == 'POST') {
					for (var key in mSendData)
						if (
							!  mSendData[key] ||
							(typeof mSendData[key] == 'object' && Object.keys(mSendData[key]).length === 0)
						) {
							mSendData[key] = '';
						}
				}
				// abort previous ajax request if not already finshed of failed
				// dont abort in batch mode
				var sXHRid = this.getServiceUrl()+JSON.stringify(mSendData);
				if (sXHRid in this._jqXhr)
					this._jqXhr[sXHRid].abort();
				// proxy self reference
				var that = this;
				this._jqXhr[sXHRid] = _static.POST(this, 
					this.getServiceUrl(),
					mSendData,
					function(mData) {
						// empty xhr object
						delete(that._jqXhr[sXHRid]);//  = null;
						// api data gets proccessed in the correct format
						var mProcessedData = _static.processApiData(mData);
						if (mProcessedData.success) {
							that.setColumns(mProcessedData.columns);
							// update the model
							if (! bDisableModelUpdate) {}
								that.setData(mProcessedData.model);
							if (typeof fOnSuccess === 'function')
								fOnSuccess(mProcessedData.model);
						} else {
							if (typeof fOnError === 'function') {
								var strErrorMsg = mProcessedData.errorMessage || that._mSettings.defaulErrorMessage ;
								fOnError(strErrorMsg, mProcessedData.error);
							}
						}
						if (typeof fOnAllways === 'function')
							fOnAllways();
					},
					'json'
				).fail(function(XMLHttpReq, textStatus, errorThrown) {
					// empty xhr object
					delete(that._jqXhr[sXHRid]);//  = null;
					if (textStatus !== 'abort') {
						// log error
						jQuery.sap.log.fatal("The following problem occurred: " + textStatus, XMLHttpReq.responseText + "," + XMLHttpReq.status + "," + XMLHttpReq.statusText);
						if (typeof fOnError === 'function')
							fOnError(XMLHttpReq, textStatus, errorThrown);
						if (typeof fOnAllways === 'function')
							fOnAllways();
					}
				});
				return this._jqXhr[sXHRid];
			};

			API.prototype.reload = function(mParameters) {
				if (typeof mParameters !== 'object')
					mParameters = {};
				var success = mParameters.success || false,
					error	= mParameters.error || false,
					allways = mParameters.allways || false;
				this.send({}, success, error, allways);
			};
		
			/**
			 * Create a new entry in the model
			 * @param  object	mData      the parameters for the new entry
			 * @param  function fOnSuccess triggered on succes
			 *                             first param is the new created model
			 * @param  function fOnError   triggered on error
			 */
			API.prototype.create = function(mData, mParameters) {
				if (typeof mData !== 'object')
					return;
				if (typeof mParameters !== 'object')
					mParameters = {};
				var aColumns = this.getColumns();
				if (aColumns.length) {
					for (var keyColumns in aColumns) {
						mData[aColumns[keyColumns]] = (typeof mData === 'object' && aColumns[keyColumns] in mData) ? String(mData[aColumns[keyColumns]]) : '';

					}
				}
				// on new object creation.. set filter to 0 so that only the newly created will be returned
				if(Object.keys(this.oData).length === 0) {
					this._mSettings.mUrlParameters._filterIds = '0';
					mData['_filterIds'] = '0';
				}
				var fOnSuccessCreate = fOnSuccess;
				mData[this.getApiActionKey()] = 'create';
				var fOnSuccess	= mParameters.success || false,
					fOnError	= mParameters.error || false,
					fOnAllways  = mParameters.allways || false;
				if (typeof fOnSuccess === 'function') {
					var that = this;
					fOnSuccessCreate = function(model) {
						// the last one is the one created
						var primaryKey		= that.getPrimaryKey(),
							mSortedModel	= Object.keys(model).sort(function(keyA, keyB) {
								return model[keyB][primaryKey] - model[keyA][primaryKey];
							}),
							sPropertyKey	= mSortedModel[0],
							// loose reference
							mNewEntry		= $.extend(true, {}, model[sPropertyKey]);
						// add to filter when a filter is present
						if ('_filterIds' in that._mSettings.mUrlParameters && that._mSettings.mUrlParameters._filterIds !== false) {
							var aFilter = that._mSettings.mUrlParameters._filterIds.split(',');
							aFilter.push(mNewEntry[primaryKey]);
							that._mSettings.mUrlParameters._filterIds = aFilter.join(',');
						}
						// success callback
						fOnSuccess(mNewEntry, sPropertyKey);
					};
				}
				return _static.batchOrSend(this, mData, fOnSuccessCreate, fOnError, fOnAllways);
			};

			API.prototype.delete = function(key, mParameters) {
				if (! key)
					return;
				if (typeof mParameters !== 'object')
					mParameters = {};
				var mData		= {},
					fOnSuccess	= mParameters.success || false,
					fOnError	= mParameters.error || false,
					fOnAllways	= mParameters.allways || false;
				mData[this.getPrimaryKey()]	= key;
				mData[this.getApiActionKey()] = 'delete';
				var mToDelete = this.getProperty('/'+key);
				if (! mToDelete) {
					_static.log('delete: can\'t delete model with key '+mData[this.getPrimaryKey()]+' not in current model');
					if (typeof fOnError == 'function')
						fOnError();
				} else	{
					return _static.batchOrSend(this, mData, fOnSuccess, fOnError, fOnAllways);
				}
			};

			/** 
			 * can use this in method in 3 ways: 
			 * @param number key
			 * @param object mData object with new values
			 * @param object mParameters
			 *		mParameters.success		a callback function which is called when the data has been successfully updated.
			 *		mParameters.error		a callback function which is called when the request failed.
			 */
			API.prototype.update = function(key, mData, mParameters) {
				if (typeof key === 'object') {
					mParameters	= mData;
					mData		= key;
				} else {
					mData = (typeof mData === 'object') ? mData : {} ;
					mData[this.getPrimaryKey()] = key;
				}
				// loose reference
				mData = $.extend(true, {}, mData);
				mData[this._mSettings.apiActionKey] = 'update';
				if (! mData[this.getPrimaryKey()])
					return;
				var fOnSuccess	= (mParameters) ? mParameters.success || false : false,
					fOnError	= (mParameters) ? mParameters.error || false : false,
					fOnAllways	= (mParameters) ? mParameters.allways || false : false;
				var mToUpdate = this.getProperty('/'+mData[this.getPrimaryKey()]);
				if (! mToUpdate) {
					_static.log('update: can\'t update model with key '+mData[this.getPrimaryKey()]+' not in current model');
					if (typeof fOnError == 'function')
						fOnError();
				} else	{
					return _static.batchOrSend(this, mData, fOnSuccess, fOnError, fOnAllways);
				}
			};

			// API.prototype.customCall = function(mData, mParameters) {
				
			// 	// loose reference
			// 	mData = $.extend(true, {}, mData);
			// 	mData[this._mSettings.apiActionKey] = 'customcall';
			// 	if (! mData[this.getPrimaryKey()])
			// 		return;
			// 	var fOnSuccess	= (mParameters) ? mParameters.success || false : false,
			// 		fOnError	= (mParameters) ? mParameters.error || false : false,
			// 		fOnAllways	= (mParameters) ? mParameters.allways || false : false;
			// 	console.log(this.__API.getServiceUrl());
			// 	_static.POST(this.__API,
			// 		this.__API.getServiceUrl(),
			// 		mData,
			// 		function(mResponse) {
			// 			if (mResponse.success && (typeof fOnSuccess == 'function')) {
			// 				delete mResponse.success;
			// 				fOnSuccess(mResponse);
			// 			}else if (typeof fOnError === 'function') {
			// 				fOnError(XMLHttpReq, textStatus, errorThrown);
			// 			} if (typeof fOnAllways === 'function') {
			// 				fOnAllways();
			// 			}
			// 		},
			// 		'json'
			// 	).fail(function(XMLHttpReq, textStatus, errorThrown) {
			// 		if (textStatus !== 'abort') {
			// 			// log error
			// 			jQuery.sap.log.fatal("The following problem occurred: " + textStatus, XMLHttpReq.responseText + "," + XMLHttpReq.status + "," + XMLHttpReq.statusText);
			// 			if (typeof fOnError === 'function')
			// 				fOnError(XMLHttpReq, textStatus, errorThrown);
			// 			if (typeof fOnAllways === 'function') {
			// 				fOnAllways();
			// 			}
			// 		}
			// 	});


			// };

			// return API so that com.barrydam.sap.ui.model.API refers to API
			return API;
		},
		true // bExport	
	);

})(jQuery, window);