 (function () {
	jQuery.sap.declare("nl.alfasan.customerrevenue.custom.Link");
	sap.m.Link.extend("nl.alfasan.customerrevenue.custom.Link", {
			metadata: {
				events: {
					"hover": {}
				}
			},
			
			onmouseover: function(evt){
				this.fireHover({type:"over"});
			},
			
			onmouseout: function(evt) {
			    this.fireHover({type:"out"});
			},
			
			renderer:{}
			
		});
		
}());